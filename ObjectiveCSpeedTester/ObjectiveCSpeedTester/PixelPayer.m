//
//  PixelLayer - Objective-C version
//

#import "PixelLayer.h"

typedef struct {
    UInt8 alpha;
    UInt8 red;
    UInt8 green;
    UInt8 blue;
} ARGBValue;

@interface PixelLayer() {
    CVPixelBufferRef pixelBuffer;
    UInt8 *pixelBufferBase;
}

@property int width;
@property int height;

@end


@implementation PixelLayer

- (instancetype)initWithWidth:(int)width height:(int)height {
    
    if (self = [super init]) {
        self.width = width;
        self.height = height;
    }
    
    // Create our pixel buffer
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, self.width, self.height, kCVPixelFormatType_32ARGB, nil, &pixelBuffer);
    if (status != kCVReturnSuccess) {
        NSLog(@"! Unable to create pixel buffer");
        return nil;
    }
    
    // Lock it for writing
    [self lockForWriting];
    
    return self;
}

- (void)runWriteTest:(int)count {
    NSLog(@"> Running %d Objective-C speed tests…", count);
    double startStamp = NSDate.timeIntervalSinceReferenceDate;
    for (int loop = 0; loop<count; loop++) {
        [self writeTest];
    }
    double endStamp = NSDate.timeIntervalSinceReferenceDate;
    NSLog(@"  - Ticks: %f", (endStamp - startStamp));
}

- (void)writeTest {
    ARGBValue dangerARGB = { 255, 200, 20, 20 };
    for (int height=0; height < self.height; height++) {
        for (int width=0; width < self.width; width++) {
            [self setRGBValue:dangerARGB AtX:width Y:height];
        }
    }
}

- (void)setRGBValue:(ARGBValue)destRGB AtX:(int)x Y:(int)y {
    unsigned long pixelOffset = (y * self.width * 4) + (x * 4);
    pixelBufferBase[pixelOffset    ] = destRGB.alpha;
    pixelBufferBase[pixelOffset + 1] = destRGB.red;
    pixelBufferBase[pixelOffset + 2] = destRGB.green;
    pixelBufferBase[pixelOffset + 3] = destRGB.blue;
}

- (bool)lockForWriting {
    CVReturn status = CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    if (status != kCVReturnSuccess) {
        NSLog(@"! Unable to lock pixel buffer for writing");
        return false;
    }

    // Set our pixel buffer base
    pixelBufferBase = CVPixelBufferGetBaseAddress(pixelBuffer);
    
    return true;
}

- (bool)unlock {
    CVReturn status = CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    if (status != kCVReturnSuccess) {
        NSLog(@"! Unable to unlock pixel buffer");
        return false;
    }
    
    return true;
}

- (void)dealloc {
    [self unlock];
    CVPixelBufferRelease(pixelBuffer);
}

@end
