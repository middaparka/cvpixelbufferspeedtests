//
//  main.m
//  ObjectiveCSpeedTester
//
//  Created by John Parker on 10/04/2022.
//

#import <Foundation/Foundation.h>
#import "PixelLayer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"> Running Objective-C speed tests…");
        PixelLayer *pixelLayer = [[PixelLayer alloc] initWithWidth:640 height:640];
        [pixelLayer runWriteTest:1000];
    }
    return 0;
}

