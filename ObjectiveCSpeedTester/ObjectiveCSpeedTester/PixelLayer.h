//
//  PixelLayer - Objective-C version
//

#import <Foundation/Foundation.h>
#import <CoreVideo/CoreVideo.h>

@interface PixelLayer : NSObject

- (instancetype)initWithWidth:(int)width height:(int)height;
- (void)runWriteTest:(int)count;

@end

