//
//  PixelLayer - UnsafeMutableRawPointer version.swift
//

import Foundation
import VideoToolbox

class PixelLayer_UnsafeMutableRawBufferPointer {

    var pixelBuffer: CVPixelBuffer? = nil
    var pixelBufferBase: UnsafeMutableRawPointer? = nil
    var byteBuffer: UnsafeMutableRawBufferPointer? = nil
    let width: Int
    let height: Int

    init?(_width width : Int, height : Int) {

        self.width = width
        self.height = height
        
        // Create our pixel buffer
        let status: CVReturn = CVPixelBufferCreate(kCFAllocatorDefault, self.width, self.height, OSType(kCVPixelFormatType_32ARGB), nil, &self.pixelBuffer)
        guard (status == kCVReturnSuccess) else {
            print("! Unable to create pixel buffer")
            return nil
        }
        
        // Lock it for writing
        guard (self.lockForWriting()) else {
            print("! Unable to lock pixel buffer for writing")
            return nil
        }
    }
        
    func runWriteTest(count: Int) {
        print("> Running \(count) PixelLayer_UnsafeMutableRawBufferPointer speed tests…")
        let startStamp : Double = NSDate().timeIntervalSince1970
        for _ in 1...100 {
            self.writeTest()
        }
        let endStamp : Double = NSDate().timeIntervalSince1970
        print("  - Ticks: \( endStamp - startStamp )")
    }
    
    private func writeTest() {
        let drawWidth = self.width
        let drawHeight = self.height
        let dangerARGB : ARGBValue = ARGBValue(red: 255, green: 0, blue: 0)
        
        for y in 1...drawHeight {
            for x in 1...drawWidth {
                self.setPixelColourAtX(x, y: y, colour: dangerARGB)
            }
        }
    }
    
    public func setPixelColourAtX(_ x: Int, y: Int, colour: ARGBValue) -> Void {
        let pixelOffset : Int = ((y - 1) * self.width * 4) + ((x - 1) * 4)
        self.byteBuffer![ pixelOffset     ] = colour.alpha
        self.byteBuffer![ pixelOffset + 1 ] = colour.red
        self.byteBuffer![ pixelOffset + 2 ] = colour.green
        self.byteBuffer![ pixelOffset + 3 ] = colour.blue
    }
        
    private func lockForWriting() -> Bool {
        let status: CVReturn = CVPixelBufferLockBaseAddress(self.pixelBuffer!, CVPixelBufferLockFlags.init())
        guard (status == kCVReturnSuccess) else {
            print("! Unable to lock pixel buffer for writing");
            return false
        }

        // Set our pixel buffer base and point the byte buffer at it
        self.pixelBufferBase = CVPixelBufferGetBaseAddress(self.pixelBuffer!)
        self.byteBuffer = UnsafeMutableRawBufferPointer(start: self.pixelBufferBase, count: self.width * self.height * 4)
        
        return true
    }
   
    private func unlock() -> Bool {
        let status: CVReturn = CVPixelBufferUnlockBaseAddress(self.pixelBuffer!, CVPixelBufferLockFlags.init())
        guard (status == kCVReturnSuccess) else {
            return false
        }
        
        self.pixelBufferBase = nil
        self.byteBuffer = nil
        
        return true
    }
    
    func destroy() {
        guard (self.unlock()) else {
            print("! Unable to unlock pixel buffer")
            return
        }
    }
}
