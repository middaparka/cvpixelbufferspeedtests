//
//  main.swift
//  SwiftSpeedTester
//
//  Created by John Parker on 10/04/2022.
//

import Foundation

// Shared struct across testers
struct ARGBValue {
    let alpha: UInt8
    let red: UInt8
    let green: UInt8
    let blue: UInt8

    init(red: UInt8, green: UInt8, blue: UInt8) {
        self.alpha = 255
        self.red   = red
        self.green = green
        self.blue  = blue
    }
}

func runTests() -> Void {
    guard let unsafeMutablePixelLayer = PixelLayer_UnsafeMutablePointer(_width: 640, height: 640)  else {
        print("! Unable to create PixelLayer_UnsafeMutablePointer…")
        return
    }
    unsafeMutablePixelLayer.runWriteTest(count: 1000)
    
    guard let unsafeMutableRawBufferPixelLayer = PixelLayer_UnsafeMutableRawBufferPointer(_width: 640, height: 640) else {
        print("! Unable to create PixelLayer_UnsafeMutableRawBufferPointer…")
        return
    }
    unsafeMutableRawBufferPixelLayer.runWriteTest(count: 1000)
}

print("> Running Swift speed tests…")
runTests()
